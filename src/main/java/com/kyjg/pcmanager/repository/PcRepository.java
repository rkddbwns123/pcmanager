package com.kyjg.pcmanager.repository;

import com.kyjg.pcmanager.entity.Pc;
import org.springframework.data.jpa.repository.JpaRepository;
public interface PcRepository extends JpaRepository<Pc,Long> {
}
