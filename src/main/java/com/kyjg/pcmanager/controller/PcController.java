package com.kyjg.pcmanager.controller;

import com.kyjg.pcmanager.model.PcRequest;
import com.kyjg.pcmanager.service.PcService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pc")
public class PcController {
    private final PcService pcService;
    @PostMapping("/data")
    public String setPc(@RequestBody PcRequest request) {
        pcService.setPc(request.getComputerId(), request.getManagerName());
    return "OK";
    }
}
