package com.kyjg.pcmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PcRequest {
    private String computerId;
    private String managerName;
}
