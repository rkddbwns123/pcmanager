package com.kyjg.pcmanager.service;

import com.kyjg.pcmanager.entity.Pc;
import com.kyjg.pcmanager.repository.PcRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PcService {
    private final PcRepository pcRepository;

    public void setPc (String computerId, String managerName) {
        Pc addData = new Pc();

        addData.setComputerId(computerId);
        addData.setManagerName(managerName);

        pcRepository.save(addData);
    }
}
